from othello import *

def est_coin(ligne, colonne):
    return (ligne == 0 or ligne == 7) and (colonne == 0 or colonne == 7)

#heuristique simple : +1 par case à sa couleur, -1 par case à la couleur adverse
def heuristique_1(othello, joueur):
    valeur = 0
    plateau = othello.plateau
    for ligne in range(8):
        for colonne in range(8):
            case = plateau.get_case(ligne, colonne)
            if case.est_occupee():
                piece = case.get_contenu();
                if piece.est_semblable(joueur):
                    valeur += 1
                else:
                    valeur -= 1
    return valeur

#heuristique un peu plus évoluée : les pièces dans les angles comptent pour 5 au lieu de 1
def heuristique_2(othello, joueur):
    valeur = 0
    plateau = othello.plateau
    for ligne in range(8):
        for colonne in range(8):
            case = plateau.get_case(ligne, colonne)
            if case.est_occupee():
                piece = case.get_contenu();
                if piece.est_semblable(joueur):
                    if est_coin(ligne, colonne):
                        valeur += 5
                    else:
                        valeur += 1
                else:
                    if est_coin(ligne, colonne):
                        valeur -= 5
                    else:
                        valeur -= 1
    return valeur
