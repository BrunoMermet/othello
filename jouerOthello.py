from othello import *
from alphabetaObjet import *
import curses
import sys
import time

TEMPO1 = 0.01
TEMPO2 = 0.2

def initCurses():
    stdscr= curses.initscr()
    curses.start_color()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.noecho()
    curses.cbreak()
    stdscr.keypad(True)
    return stdscr

def endCurses(stdscr):
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()

def affiche_othello_curses(othello, ecran):
    lignes = str(othello).split("\n")
    for i in range(len(lignes)):
        ecran.addstr(curses.LINES - 24 + i, 0, lignes[i])
    ecran.refresh()

def get_afficheur_stat(ecran):
#    def afficheur_stat_globales(meilleure_suite, meilleure_valeur, nb_situations):
    def afficheur_stat_globales(nb_situations):
        #ecran.addstr(curses.LINES - 5, 0, "Meilleure suite envisagée : " + str(meilleure_suite))
        #ecran.addstr(curses.LINES - 4, 0, "Valeur de la meilleure suite envisagée : " + str(meilleure_valeur))
        ecran.addstr(curses.LINES - 3, 0, "Nombre de situations étudiées : " + str(nb_situations)+"    ")
        ecran.refresh()
    def afficheur_stat_locales(coups, valeur):
        ecran.addstr(curses.LINES - 2, 0, "Liste de coups actuellement examinée : " + str(coups))
        ecran.addstr(curses.LINES - 1, 0, "Valeur de la situation atteinte : " + str(valeur)+"   ")
        ecran.refresh()
    return afficheur_stat_globales, afficheur_stat_locales

def tour_humain(othello):
    print(othello)
    case_valide= False
    sortir = False
    while not case_valide and not sortir:
        ligne_correcte = False
        while not ligne_correcte:
            try:
                saisie = input("Ligne où jouer : ")
                if saisie == "q":
                    sortir = True
                    break
                ligne = int(saisie)
                ligne_correcte  = True
            except:
                pass
        colonne_correcte = False
        if not sortir:
            while not colonne_correcte:
                try:
                    saisie = input("Colonne où jouer : ")
                    if saisie == "q":
                        sortir = True
                        break
                    colonne = int(saisie)
                    colonne_correcte = True
                except:
                    pass
            if not sortir:
                case_valide = othello.case_valide(ligne, colonne)
                if not case_valide:
                    print("Case invalide")
    if not sortir:
        othello.jouer_coup(ligne,colonne)
    return sortir

def tour_ordi(othello, PROF, heuristique):
    global TEMPO1, TEMPO2
    try:
        ecran = initCurses()
        affiche_othello_curses(othello, ecran)
        if PROF > 6:
            TEMPO1 = 0
            TEMPO2 = 0
        algo = AlphaBeta(PROF, heuristique, get_afficheur_stat(ecran), TEMPO1, TEMPO2)
        solution = algo.executer(othello)
        ecran.getch()
    finally:
        endCurses(ecran)
    print("Liste de coups sélectionnée :",solution[0])
    print("Valeur espérée :", solution[1])
    coup = solution[0][0]
    print("On joue donc : (" + str(coup[0]) + "," + str(coup[1]) + ")")
    othello.jouer_coup(coup[0], coup[1])

def main(PROF, heurisitique):
    othello = Othello()
    for i in range(32):
        if othello.joueur_courant == "noir":
            tour_ordi(othello, PROF, heuristique)
        else:
            sortir = tour_humain(othello)
            if sortir:
                print("Au revoir...")
                break
            
if __name__ == "__main__":
    if len(sys.argv) > 3 or (len(sys.argv) == 2 and (sys.argv[1] in ["-h", "--help"])):
        print("usage : othello [ profondeur [heuristique]]")
        print("  heuristique : h1 ou h2")
        exit()
    if len(sys.argv) >= 2:
        PROF = int(sys.argv[1])
    else:
        PROF = 5
    if len(sys.argv) >= 3:
        if (sys.argv[2]) == "h2":
            heurisitque = heuristique_2
        else:
            heuristique = heuristique_1
    else:
        heuristique = heuristique_2
    main(PROF, heuristique)
