# Othello

## Introduction
Embryon de programme pour jouer à Othello. L'ordinateur joue avec un
algorithme alpha-beta. 2 heuristiques très simplistes sont
proposées. il est possible de passer en paramètre la profondeur de la
recherche. La fin de partie n'est pas gérée.

## Avertissement
La partie "Graphisme texte" a été écrite à la ramasse juste avant une fête de la science alors que j'avais plein d'autres choses à faire. Du coup, c'est assez sale. J'essaierai d'améliorer cela à l'occasion.