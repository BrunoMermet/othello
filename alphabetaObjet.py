from heuristiquesOthello import *
import time

class AlphaBeta:
    def __init__(self, prof_max, heuristique, afficheur_stat, TEMPO1, TEMPO2):
        self.prof_max = prof_max
        self.heuristique = heuristique
        self.nb_situations_examinees = 0
        self.afficheur_stat_globales = afficheur_stat[0]
        self.afficheur_stat_locales = afficheur_stat[1]
        self.meilleure_recompense = float('-inf')
        self.meilleur_enchainement = []
        self.TEMPO1 = TEMPO1
        self.TEMPO2 = TEMPO2

    def tour_joueur_courant(self, prof_courante):
        return prof_courante % 2 == 1

    def prof_max_atteinte(self, prof_courante):
        return prof_courante > self.prof_max

    def get_nb_situations_examinees(self):
        return self.nb_situations_examinees

    def executer(self, situation):
        self.joueur = situation.joueur_courant
        self.nb_situations_examinees = 0
        return self.alpha_beta(situation, 1, float('-inf'), float('inf'))
        
    def alpha_beta(self, situation, prof_courante, alpha, beta, trace = None):
        pauser = False
        if trace is None:
            trace = []
        self.nb_situations_examinees += 1
        if self.prof_max_atteinte(prof_courante):
            time.sleep(self.TEMPO1)
            retour = ([], self.heuristique(situation, self.joueur))
            self.afficheur_stat_locales(trace, retour[1])
            if retour[1] > self.meilleure_recompense:
                self.meilleure_recompense = retour[1]
                self.meilleur_enchainement = trace
                pauser = True
            #self.afficheur_stat_globales(self.meilleur_enchainement, self.meilleure_recompense, self.nb_situations_examinees)
            self.afficheur_stat_globales(self.nb_situations_examinees)
            if pauser:
                time.sleep(self.TEMPO2)
                pauser = False
        else:
            possibilites = situation.liste_coups()
            if len(possibilites) == 0:
                retour = self.evaluer_passer_son_tour(situation, prof_courante, alpha, beta, trace)
            else:
                if self.tour_joueur_courant(prof_courante):
                    retour = self.retenir_meilleur_coup(situation, possibilites, prof_courante, alpha, beta, trace)
                else:
                    retour = self.retenir_pire_coup(situation, possibilites, prof_courante, alpha, beta, trace)
        return retour
            
    def evaluer_passer_son_tour(self, situation, prof_courante, alpha, beta, trace):
        nouvelle_situation = Othello(situation)
        nouvelle_situation.changer_joueur()
        enchainement, recompense = self.alpha_beta(nouvelle_situation, prof_courante+1, alpha, beta, trace + ["PASS"])
        retour = (([None] + enchainement), recompense)
        return retour

    def retenir_meilleur_coup(self, situation, possibilites, prof_courante, alpha, beta, trace):
        coup_retenu = None
        recompense_retenue = float('-inf')
        enchainement_retenu = None
        for coup in possibilites:
            nouvelle_situation = situation.tenter_coup(coup)
            liste_coups, recompense = self.alpha_beta(nouvelle_situation, prof_courante+1, alpha, beta, trace + [coup])
            if recompense > recompense_retenue:
                recompense_retenue = recompense
                enchainement_retenu = liste_coups
                coup_retenu = coup
                if recompense_retenue >= beta:
                    break
            if recompense > alpha:
                alpha = recompense

        return [coup_retenu] + enchainement_retenu, recompense_retenue
    
    def retenir_pire_coup(self, situation, possibilites, prof_courante, alpha, beta, trace):
        coup_retenu = None
        recompense_retenue = float('inf')
        enchainement_retenu = None
        for coup in possibilites:
            nouvelle_situation = situation.tenter_coup(coup)
            liste_coups, recompense = self.alpha_beta(nouvelle_situation, prof_courante+1, alpha, beta, trace + [coup])
            if recompense < recompense_retenue:
                recompense_retenue = recompense
                enchainement_retenu = liste_coups
                coup_retenu = coup
                if recompense_retenue <= alpha:
                    break
            if recompense < beta:
                beta = recompense
        return [coup_retenu] + enchainement_retenu, recompense_retenue
    
